FROM gitlab-registry.cern.ch/cloud/puppetbase:latest
MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

RUN yum install puppet-agent sudo initscripts -y && \
    yum clean all

CMD [ "/opt/puppetlabs/bin/puppet", "agent", "-t", "--verbose" ]

